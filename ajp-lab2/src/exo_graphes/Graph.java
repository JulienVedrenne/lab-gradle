package exo_graphes;

import java.util.HashMap;
import java.util.Set;

public interface Graph<V> {
	boolean addEdge (V fromVertex ,V toVertex);
	boolean addVertex (V vertex);
	Set<V> getChildren(V vertex);
	public HashMap<V,Set<V>> getHm();
	public Set<Word> addVertices(Set<? extends Word> vertices) ;
}
