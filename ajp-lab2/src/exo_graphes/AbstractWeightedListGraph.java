package exo_graphes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import exo_graphes.AbstractWeightedListGraph.Tuple;

public abstract class AbstractWeightedListGraph<V,E> extends AbstractListGraph<V>  implements WeightedGraph<V,E> {
	
	protected final Map<Tuple ,E> edges=new  HashMap<Tuple,E>();
	
	
	class Tuple{
		private V fromVertex ;
		private V toVertex;
		
		public Tuple(V fromVertex, V toVertex) {
			super();
			this.fromVertex = fromVertex;
			this.toVertex = toVertex;
		}

		@Override
		public String toString() {
			return "Tuple [fromVertex=" + fromVertex + ", toVertex=" + toVertex + "]";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((fromVertex == null) ? 0 : fromVertex.hashCode());
			result = prime * result + ((toVertex == null) ? 0 : toVertex.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Tuple other = (Tuple) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (fromVertex == null) {
				if (other.fromVertex != null)
					return false;
			} else if (!fromVertex.equals(other.fromVertex))
				return false;
			if (toVertex == null) {
				if (other.toVertex != null)
					return false;
			} else if (!toVertex.equals(other.toVertex))
				return false;
			return true;
		}

		private AbstractWeightedListGraph getOuterType() {
			return AbstractWeightedListGraph.this;
		}
		
		
		
		
		
	}

	public boolean addVertex(AbstractWeightedListGraph<V, E>.Tuple t) {
		// TODO Auto-generated method stub
		return false;
	}
	
	protected String edgeString(V fromVertex , V toVertex){
		StringBuilder strbuild = new StringBuilder();
		Tuple tup = new Tuple(fromVertex,toVertex); 
		
		
		if(edges.containsKey(tup)){
		
		
		strbuild.append(fromVertex.toString() + " -> " + toVertex.toString() + " [label=" +edges.get(tup).toString()+"];" +System.lineSeparator());
		}
		

              return strbuild.toString();
	
	}



	
	
}
