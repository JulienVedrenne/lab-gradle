package exo_graphes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


public class WeightedListGraph<V,E> extends AbstractWeightedListGraph<V,E> implements WeightedGraph<V,E> {



	@Override
	public boolean addEdge(V fromVertex, V toVertex, E edge) {
		Tuple tup = new Tuple(fromVertex,toVertex); 
		super.edges.put(tup, edge);
		 getChildren(fromVertex).add(toVertex);
		
		
		return false;
	}

	@Override
	public boolean addEdge(V fromVertex, V toVertex) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addVertex(V vertex) {
		  Set< V > edges = new LinkedHashSet<>();
			hm.put(vertex,edges);
		
		return true;
	}

	@Override
	public Set<V> getChildren(V vertex) {
		return hm.get(vertex);
	}

	@Override
	public HashMap<V, Set<V>> getHm() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public static void main(String[] args) {
		WeightedListGraph<String,Integer> graph= new WeightedListGraph<>();
		
		String s1="a";
		String s2="b";
		String s3="c";
		String s4="d";
		String s5="e";
		String s6="f";
		
		graph.addVertex(s1);
		graph.addVertex(s2);
		graph.addVertex(s3);
		graph.addVertex(s4);
		graph.addVertex(s5);
		graph.addVertex(s6);
		graph.addEdge(s1, s2,2);
		graph.addEdge(s2, s3,3);
		graph.addEdge(s2, s5,4);
		graph.addEdge(s3, s4,5);
		graph.addEdge(s3, s1,2);
		graph.addEdge(s3, s6,7);
		graph.addEdge(s4, s2,8);
		
		String st = graph.getDotType();
		System.out.println(st);
	}

	@Override
	protected String getDotType() {
		 StringBuilder strbuild = new StringBuilder("digraph G {" + System.lineSeparator());
	        for(Map.Entry<V, Set<V>> map : hm.entrySet()) {
	        	strbuild.append("node ");
	        	strbuild.append("[label=" + map.getKey().toString() + "] ");
	        	strbuild.append(map.getKey().hashCode() + System.lineSeparator());
	        }
	        for(Map.Entry<V, Set<V>> set : hm.entrySet()) {
	            for(V v : set.getValue())
	            	
	            	strbuild.append(this.edgeString(set.getKey(), v));
	      
	        }
	        strbuild.append("}");
	        return strbuild.toString();
	    
	}

}
