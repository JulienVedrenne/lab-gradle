package exo_graphes;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

public abstract class AbstractListGraph<V> implements Graph<V> {
	
	protected HashMap<V,Set<V>> hm = new HashMap<>();

	@Override
	public boolean addVertex(V vertex) {
		Set<V> edges = new LinkedHashSet<>();
		hm.put(vertex,edges);
		return true;
	}
	

	@Override
	public Set<V> getChildren(V vertex) {
		return hm.get(vertex);
	}
	
	@Override
	public HashMap<V,Set<V>> getHm() {
		return hm;
	}

	public void setHm(HashMap<V,Set<V>> hm) {
		this.hm = hm;
	}
	
	protected abstract String getDotType();
	
	protected HashMap<V,Set<V>> getUniqueAdjacencies()
	{
		return this.hm;
	}
}
