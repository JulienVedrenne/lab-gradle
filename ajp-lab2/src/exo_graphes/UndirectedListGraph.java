package exo_graphes;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.Iterator;

public class UndirectedListGraph<V> extends AbstractListGraph<V> {
	 
	@Override
	public boolean addEdge(V fromVertex, V toVertex) {	
		getChildren(fromVertex).add(toVertex);
		getChildren(toVertex).add(fromVertex);
		return true;
	}
	
	@Override
    public String getDotType() {
        StringBuilder strbuild = new StringBuilder("digraph G {" + System.lineSeparator());
        for(Map.Entry<V, Set<V>> map : hm.entrySet()) {
        	strbuild.append("node ");
        	strbuild.append("[label=" + map.getKey().toString() + "] ");
        	strbuild.append(map.getKey().hashCode() + System.lineSeparator());
        }
        for(Map.Entry<V, Set<V>> set : hm.entrySet()) {
            for(V v : set.getValue())
            	strbuild.append(set.getKey().hashCode() + " -- " + v.hashCode() + System.lineSeparator());
        }
        strbuild.append("}");
        return strbuild.toString();
    }
	
	@Override
	protected HashMap<V, Set<V>> getUniqueAdjacencies()
	{
		for(Map.Entry<V, Set<V>> map : hm.entrySet()) {
        	System.out.print(map.getKey().toString());
        	//for (Set<V> temp : map.getValue())
        	System.out.println(map.getValue().toString());
      
        }
		
		return hm;
	}



	@Override
	public Set<V> addVertices(Set<V> vertices) {
		// TODO Auto-generated method stub
		return null;
	}
}
