package exo_graphes;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class DirectedListGraph<V> extends AbstractListGraph<V> {
	 
	@Override
	public boolean addEdge(V fromVertex, V toVertex) {	
		 getChildren(fromVertex).add(toVertex);
		return true;
	}
	
	
	@Override
    public String getDotType() {
        StringBuilder strbuild = new StringBuilder("digraph G {" + System.lineSeparator());
        for(Map.Entry<V, Set<V>> map : hm.entrySet()) {
        	strbuild.append("node ");
        	strbuild.append("[label=" + map.getKey().toString() + "] ");
        	strbuild.append(map.getKey().hashCode() + System.lineSeparator());
        }
        for(Map.Entry<V, Set<V>> set : hm.entrySet()) {
            for(V v : set.getValue())
            	strbuild.append(set.getKey().hashCode() + " -> " + v.hashCode() + System.lineSeparator());
        }
        strbuild.append("}");
        return strbuild.toString();
    }


	@Override
	public Set<Word> addVertices(Set<? extends Word> vertices) {
		 Set<Word> list = new LinkedHashSet<>();
		for (Word s : vertices) {
		  if(hm.containsKey(s)){
			 list.add(s);
		  }else{
			  Set<Word> edges = new LinkedHashSet<>();
			//hm.put(s,edges);
		  }
		}
		return list;
	}



}
